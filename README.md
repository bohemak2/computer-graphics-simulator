Simulator of computer graphic, written in JOGL. It takes parameters in diff axis for modeling. Sample of textures, lights and normals.
![Wire model and normals](https://i.ibb.co/hLxppNc/Sn-mek-obrazovky-7.png)
![Spot light in circle-universe](https://i.ibb.co/CmNGLKH/Sn-mek-obrazovky-8.png)