#version 150
uniform mat4 proj;
uniform mat4 view;

uniform float size;
uniform int sphere;

in vec2 inPosition;
in vec3 inColor;
in vec2 inTextureCoordinates;

out vec3 vertColor;
out vec2 texCoord;

const float PI = 3.14;

void main() {
	vec2 position = inPosition;

	float x, y, z;
	if (sphere == 1) {
		// Round universe
		float s = (position.y * (2*PI));
		float t =  (PI * position.x);

		x = sin(t) * cos(s) * size;
		y = sin(t) * sin(s) * size;
		z = cos(t) * size;
	} else {
		// Flat universe
		x = (position.x * size) - (size / 2);
		y = (position.y * size) - (size / 2);
		z = -0.5;
	}

	texCoord = inTextureCoordinates;
	vertColor = inColor;

	gl_Position = proj * view * vec4(x, y, z, 1.0);
} 
