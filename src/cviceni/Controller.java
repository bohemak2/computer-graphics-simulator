package cviceni;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Controller {
	
	public Controller () {
		resetCameraCheckboxes();
		resetObjectsCheckboxes();
	}
	
	public void afterInit() {		
		// MODEL.CUSTOM AXIS in panel
		resetAxisLabels();
		resetAxisFields();
		
		
		// ADD to SettingsPanel
		settingsPanel = new Panel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
				
				// BASIC
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
					
					add(new Label("SPEED"));
					add(getSpeed());
				}});
		
				
				// CAMERA
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
				
					add(new Label("CAMERA"));
					add(new Panel() {/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

					{
						setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); 
						
						for (Checkbox cameraCheckbox : getCameraCheckboxes()) {
							add(cameraCheckbox);
						}
					}});
				}});
		
				// POLYGON MODE
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
					add(new Label("MODE"));
					add(new Panel() {/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

					{
						setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
						
						for (Checkbox modeCheckbox : getModeCheckboxes()) {
							add(modeCheckbox);
						}
					}});		
				}});		
				
				// COORDINATES
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
					
					add(new Label("COORDINATES"));
					add(getAxisChoice());
				}});		
				
				// OBJECTS
				add(new JSeparator());
//				add(new Label("OBJECTS"));
				
				// OBJECT.AXIS
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
					add(getObjectCheckbox(Controller.OBJECT.AXIS));
				}});

				// OBJECT.LIGHT
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
					add(getObjectCheckbox(Controller.OBJECT.LIGHT));
					
					add(new Panel() {
						
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public boolean isVisible() { 
							return isObjectVisible(OBJECT.LIGHT);
						};
						
						{
							setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
							
							add(getLightChoice());
							add(getLightShadows());
							add(getLightIsCamera());
						}
					});
				}});

				// NORMAL
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
						add(getObjectCheckbox(Controller.OBJECT.NORMAL));
						
						add(new Panel() {
							
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public boolean isVisible() { 
								return isObjectVisible(OBJECT.NORMAL);
							};
							
							{
								setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
								
								add(getNormalChoice());
							}
						});
					
					};
				});
				
				// OBJECT.MODEL
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
					
					add(getObjectCheckbox(Controller.OBJECT.MODEL));
					
					add(new Panel() {
						
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public boolean isVisible() { 
							return isObjectVisible(OBJECT.MODEL);
						};
						
						{
							setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
							add(getModelChoice());
							
							add(new Panel() {/**
								 * 
								 */
								private static final long serialVersionUID = 1L;

							{
								setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
								
								add(new Label("DENSITY"));
								add(getModelDensity());								
							}});

							add(new Panel() {/**
								 * 
								 */
								private static final long serialVersionUID = 1L;

							{
								setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
								
								add(new Label("SIZE"));
								add(getModelSize());								
							}});
							
							// OBJECT.FUNCTIONS
							add(new Label("Functions (s, t, time, PI)"));
							for (int i = 0 ; i < getAxisTextFields().length; i++) {
								TextField field = getAxisTextFields()[i];
								field.setSize(200, 20);
								Label label = getAxisLabels()[i];

								add(new Panel() {/**
									 * 
									 */
									private static final long serialVersionUID = 1L;

								{
									setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
									
									add(label);
									add(field);
								}});
							}							
						}
					});
					
					
				}});

				// OBJECT.UNIVERSE
				add(new JSeparator());
				add(new Panel() {/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

				{
					setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
					
					add(getObjectCheckbox(Controller.OBJECT.UNIVERSE));
					
					add(new Panel() {
						
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public boolean isVisible() { 
							return isObjectVisible(OBJECT.UNIVERSE);
						};
						
						{
						setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
						
//						UNIVERSE density disabled due to same program as MODEL (except CIRCLE)

//						add(new Panel() {/**
//							 * 
//							 */
//							private static final long serialVersionUID = 1L;
//
//						{
//							setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
//							
//							add(new Label("DENSITY"));
//							add(getUniverseDensity());
//						}});
						
						add(new Panel() {/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

						{
							setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
							
							add(new Label("SIZE"));
							add(getUniverseSize());
						}});
					}});
				}});
			
			}
		};
	}
	
	public void revalidate() {
		if (getSettingsPanel() != null) {
			getSettingsPanel().revalidate();
			
			for (Component c : getSettingsPanel().getComponents()) {
				c.revalidate();
			}
		}
	}
	
	/** BASICS */
	public static float STEP = 0.05f;
	
	private JSlider speed = new JSlider(-60, 60, 1) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			setToolTipText("Volume");
			setPaintTicks(true);
			setPaintTrack(true);
			addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					compiles.put(COMPILE.OBJECT_UNIVERSE, true);
				}
			});
		}
	};
	
	public static enum COMPILE { ALL, PROJECTION, OBJECT_MODEL, OBJECT_UNIVERSE, OBJECT_AXIS};
	
	private HashMap<COMPILE, Boolean> compiles = new HashMap<COMPILE, Boolean>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(COMPILE.ALL, false);
			put(COMPILE.PROJECTION, false);
			put(COMPILE.OBJECT_MODEL, false);
			put(COMPILE.OBJECT_UNIVERSE, false);
			put(COMPILE.OBJECT_AXIS, false);
		}
	};
	
	private static Dimension canvasSize = new Dimension(800, 480);
	
	private Panel settingsPanel;
	/* END */
	
	/** CAMERA */
	public static enum CAMERA { PERSPECTIVE, ORTHOGONAL };
	private CAMERA camera = CAMERA.PERSPECTIVE;
	
	private CheckboxGroup cameraCheckboxGroup = new CheckboxGroup();
	private Checkbox[] cameraCheckboxes = new Checkbox[CAMERA.values().length]; 
	
	public void resetCameraCheckboxes() {
		CAMERA[] cameras = CAMERA.values();
		for (int i = 0; i < cameras.length; i++) {
			cameraCheckboxes[i] = new Checkbox(cameras[i].toString(), cameras[i] == camera, cameraCheckboxGroup) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				{
					addItemListener(new ItemListener() {
						
						@Override
						public void itemStateChanged(ItemEvent e) {
							setCamera(CAMERA.valueOf((String)e.getItem()));
							compiles.put(COMPILE.PROJECTION, true);
						}
					});
				}
			};
		}
	}
	/* END */
	
	/**  MODE */
	public static enum MODE { WIRED, FILLED, TEXTURED };
	private MODE mode = MODE.TEXTURED;
	
	private CheckboxGroup modeCheckboxGroup = new CheckboxGroup();
	
	private HashMap<MODE, Checkbox> modeCheckboxes = new HashMap<MODE, Checkbox>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			for (MODE mode : MODE.values()){
				put(mode, new Checkbox(mode.toString(), getMode().equals(mode), modeCheckboxGroup) {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						addItemListener(new ItemListener() {
							
							@Override
							public void itemStateChanged(ItemEvent e) {
								setMode(MODE.valueOf((String)e.getItem()));
							}
						});
					}
				});				
			}
		}
	};
	/* END */
	
	/** AXIS */
	public static enum AXIS { CARTESIAN, SPHERICAL, CYLINDRICAL };
	
	private AXIS axis = AXIS.CARTESIAN;
	
	private Choice axisChoice = new Choice() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		for (AXIS axis : AXIS.values() ) {
			add(axis.toString());
		}
		
		select(axis.toString());
		
		addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				setAxis(AXIS.valueOf((String)e.getItem()));
			}
		});
	}};
	
	private class AxisTextField extends TextField {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public AxisTextField(String str) {
			super(str);
			addTextListener(new TextListener() {
				
				@Override
				public void textValueChanged(TextEvent e) {
					compiles.put(COMPILE.OBJECT_MODEL, true);
				}
			});
		}
	}
	
	private AxisTextField[] axisTextFields = new AxisTextField[] { new AxisTextField("s"), new AxisTextField("t"), new AxisTextField("0.5") };
	
	private void resetAxisFields() {		
		for (int i = 0; i < axisTextFields.length; i++) {
			axisTextFields[i].setText(getModelFunction().get(getModel()).get(getAxis())[i]);
		}

		revalidate();
	}
	
	
	private Label[] axisLabels = new Label[] { new Label(), new Label(), new Label() }; 
	
	private void resetAxisLabels() {
		switch (axis) {
			case CARTESIAN:
				axisLabels[0].setText("X");
				axisLabels[1].setText("Y");
				axisLabels[2].setText("Z");
				break;
			case SPHERICAL:
				axisLabels[0].setText("Rho");
				axisLabels[1].setText("Phi");
				axisLabels[2].setText("Theta");
			case CYLINDRICAL:
				axisLabels[0].setText("Rho");
				axisLabels[1].setText("Phi");
				axisLabels[2].setText("Z"); 
			default:
				break;
		}
		
		revalidate();
	}
	
	private TextField[] rangeTextFields = new TextField[] { new TextField(), new TextField() };
	private Label[] rangeLabels = new Label[] { new Label("s-max"), new Label("t-max") };
	
	/* END */
	
	/** LIGHTS */
	public static enum LIGHT { GLOBAL, SPOT, TONE };
	
	private Choice lightChoice = new Choice() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			for (LIGHT light : LIGHT.values() ) {
				add(light.toString());
			}	
		}
	};
	
	private Checkbox lightShadows = new Checkbox("SHADOWS") {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public boolean isVisible() {
			return isObjectVisible(OBJECT.LIGHT);
		};
		
		{
			this.setState(true);
		}
	};
	
	private Checkbox lightIsCamera = new Checkbox("AS CAMERA");
	/* END */
	
	/** NORMALS */
	public static enum NORMAL { VERTEX, PLATE };
	
	private Choice normalChoice = new Choice() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public boolean isVisible() {
			return isObjectVisible(OBJECT.NORMAL);
		};
		
		{
			for (NORMAL normal : NORMAL.values() ) {
				add(normal.toString());
			}	
		}
	};
	
	/* END */
	
	/** OBJECTS */
	public static enum OBJECT { AXIS, LIGHT, NORMAL, UNIVERSE, MODEL};
	
	private HashMap<OBJECT, Boolean> objectsVisibility = new HashMap<OBJECT, Boolean>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(OBJECT.AXIS, false);
			put(OBJECT.LIGHT, true);
			put(OBJECT.NORMAL, true);
			put(OBJECT.UNIVERSE, true);
			put(OBJECT.MODEL, true);
		}
	};
	
	private HashMap<OBJECT, Checkbox> objectsCheckboxes = new HashMap<OBJECT, Checkbox>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(OBJECT.AXIS, null);
			put(OBJECT.LIGHT, null);
			put(OBJECT.NORMAL, null);
			put(OBJECT.UNIVERSE, null);
			put(OBJECT.MODEL, null);
		}
	};
	
	public void resetObjectsCheckboxes(){
		for (OBJECT object : OBJECT.values()) {
			objectsCheckboxes.put(object, new Checkbox(object.toString(), objectsVisibility.get(object)) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				{
					addItemListener(new ItemListener() {
						
						@Override
						public void itemStateChanged(ItemEvent e) {
							OBJECT object = OBJECT.valueOf((String)e.getItem());
							
							if (object != null) {								
								objectsVisibility.put(object, !objectsVisibility.get(object));
							}
							
							revalidate();
							Controller.this.revalidate();
						}
					});
				}
			});
		}
	}
	
	
	/** GRID */		
	private JSlider universeDensity = new JSlider(2, 30, 10) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			setToolTipText("Density");
			addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					compiles.put(COMPILE.OBJECT_UNIVERSE, true);	
				}
			});
		}
	};

	private JSlider universeSize = new JSlider(5, 15, 10) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			setToolTipText("Size");
			addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					compiles.put(COMPILE.OBJECT_UNIVERSE, true);
					compiles.put(COMPILE.PROJECTION, true);
				}
			});
		}
	};	
	/* END */
	
	/** MODEL */
	public static enum MODEL { CIRCLE, CONE, FONTAIN, HELICOID, PIPE, SOMBRERO, TOP };
	
	private MODEL model = MODEL.FONTAIN;
	
	private Choice modelChoice = new Choice() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		
		for (MODEL model : MODEL.values() ) {
			add(model.toString());
		}

		select(model.toString());
		
		addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				setModel(MODEL.valueOf(e.getItem().toString()));

				
				// Reconfigure default axis
				switch (model) {
					case CIRCLE:
						setAxis(AXIS.CARTESIAN);
						break;
						
					case CONE:
						setAxis(AXIS.SPHERICAL);
						break;
						
					case FONTAIN:
						setAxis(AXIS.CARTESIAN);
						break;

					case HELICOID:
						setAxis(AXIS.CYLINDRICAL);
						break;

					case PIPE:
						setAxis(AXIS.CARTESIAN);
						break;
					
					case SOMBRERO:
						setAxis(AXIS.CYLINDRICAL);
						break;

					case TOP:
						setAxis(AXIS.SPHERICAL);
						break;
	
					default:
						break;
				}
				getAxisChoice().select(axis.toString());
				
				resetAxisFields();
			}
		});
	}};
	
	private HashMap<MODEL, HashMap<AXIS, String[]>> modelFunction = new HashMap<MODEL, HashMap<AXIS, String[]>>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			// CARTESIAN - WEB
			put(MODEL.CIRCLE, new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{
					put(AXIS.CARTESIAN, new String[]{"sin(t)*cos(s)", "sin(t)*sin(s)", "cos(t)"});
				}}
			);
			
			
			// SPHERICAL - MY 
			put(MODEL.CONE, new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{
					put(AXIS.SPHERICAL, new String[]{"s/4", "t*2*cos(time)", "1"});
				}}
			);
			
			// CARTESIAN - MY
			put(MODEL.FONTAIN, new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{ 
					put(AXIS.CARTESIAN, new String[]{"-(cos(s)+cos(t)*cos(s))/2", "(sin(s)+cos(t)*sin(s)) / 2", "sin(t)"});
				}}
			);

			// CYLINDRICAL - WEB
			put(MODEL.HELICOID, new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{ 
					put(AXIS.CYLINDRICAL, new String[]{"t/2", "s * 6", "s - 0.25"});
				}}
					);
			
			// SPHERICAL - MY
			put(MODEL.TOP, new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{ 
					put(AXIS.SPHERICAL, new String[]{"(3+cos(4*s)) / 4", "t+6*time", "s"});
				}}
			);
			
			
			// CARTESAN - WEB
			put(MODEL.PIPE, new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{ 
					put(AXIS.CARTESIAN, new String[]{"-cos(s)", "sin(s)", "t / 2 "});
				}}
			);
			
			
			// CYLINDRICAL - WEB
			put(MODEL.SOMBRERO , new HashMap<AXIS, String[]>(){/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

			{ 
					put(AXIS.CYLINDRICAL, new String[]{"t/2", "s ", "sin(t*2)+PI/2"});
				}}
			);
		}
	};
	
	private JSlider modelDensity = new JSlider(3, 50, 25) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			setToolTipText("Density");
			addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					compiles.put(COMPILE.OBJECT_MODEL, true);
				}
			});
		}
	};

	private JSlider modelSize = new JSlider(10, 20, 15) {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			setToolTipText("Size");
			addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					compiles.put(COMPILE.OBJECT_MODEL, true);
				}
			});
		}
	};
	/* END */
	 
	/* GENERATED ALL GETTERS */
	public AXIS getAxis() {
		return axis;
	}
	
	public int getAxisInt() {
		switch (axis) {
			case CARTESIAN:
				return 1;
			case SPHERICAL:
				return 2;
			case CYLINDRICAL:
				return 3;
			
			default:
				return 0;
		}
	}

	public Choice getAxisChoice() {
		return axisChoice;
	}

	public TextField[] getAxisTextFields() {
		return axisTextFields;
	}

	public Label[] getAxisLabels() {
		return axisLabels;
	}

	public int getLightInt() {
		switch (LIGHT.valueOf(lightChoice.getSelectedItem())) {
			case GLOBAL:
				return 1;
			case SPOT:
				return 2;
			case TONE:
				return 3;
			
			default:
				return 0;
		}
	}
	
	public Choice getLightChoice() {
		return lightChoice;
	}
	
	public Checkbox getLightShadows() {
		return lightShadows;
	}

	public Checkbox getLightIsCamera() {
		return lightIsCamera;
	}

	public Choice getNormalChoice() {
		return normalChoice;
	}
	
	public int getNormalInt() {
		switch (NORMAL.valueOf(normalChoice.getSelectedItem())) {
			case VERTEX:
				return 1;
			case PLATE: 
				return 2;
				
			default:
				return 0;
		}
	}

	
	public TextField[] getRangeTextFields() {
		return rangeTextFields;
	}

	public Label[] getRangeLabels() {
		return rangeLabels;
	}

	public MODEL getModel() {
		return model;
	}

	public Choice getModelChoice() {
		return modelChoice;
	}

	public HashMap<MODEL, HashMap<AXIS, String[]>> getModelFunction() {
		return modelFunction;
	}	
	
	public CAMERA getCamera() {
		return camera;
	}
	public Checkbox[] getCameraCheckboxes() {
		return cameraCheckboxes;
	}
	
	public boolean isObjectVisible(OBJECT object) {
		return objectsVisibility.get(object);
	}
	
	
	public Checkbox getObjectCheckbox(OBJECT object) {
		return objectsCheckboxes.get(object);
	}

	private void setMode(MODE mode) {
		this.mode = mode;
	}		

	public JSlider getModelDensity() {
		return modelDensity;
	}
	
	public JSlider getModelSize() {
		return modelSize;
	}

	public JSlider getUniverseDensity() {
		return universeDensity;
	}
	
	public JSlider getUniverseSize() {
		return universeSize;
	}

	public Panel getSettingsPanel() {
		return settingsPanel;
	}
	
	public static Dimension getCanvasSize() {
		return canvasSize;
	}
	
	public JSlider getSpeed() {
		return speed;
	}
		
	/* GENERATE SOME SETTTERS */


	public MODE getMode() {
		return mode;
	}

	public Collection<Checkbox> getModeCheckboxes() {
		return modeCheckboxes.values();
	}

	public void setAxis(AXIS axis) {
		if (this.axis != axis ) {
			this.axis = axis;
			resetAxisLabels();
			compiles.put(COMPILE.OBJECT_MODEL, true);			
		}
	}

	public void setModel(MODEL model) {
		if (this.model != model) {
			compiles.put(COMPILE.OBJECT_MODEL, true);
			this.model = model;
		}
	}

	public void setCamera(CAMERA camera) {
		this.camera = camera;
	}

	public HashMap<COMPILE, Boolean> getCompiles() {
		return compiles;
	}

	public static void setCanvasSize(Dimension canvasSize) {
		Controller.canvasSize = canvasSize;
	}
	
	
	class Panel extends JPanel{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		{
			setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 1));
		}
	}
}
