package cviceni;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import cviceni.Controller.CAMERA;
import cviceni.Controller.COMPILE;
import cviceni.Controller.OBJECT;
import cviceni.helpers.GridFactory;
import cviceni.helpers.Locations;
import oglutils.OGLBuffers;
import oglutils.OGLRenderTarget;
import oglutils.OGLTextRenderer;
import oglutils.OGLTexture2D;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4OrthoRH;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

public class Renderer implements GLEventListener, MouseListener, MouseMotionListener, MouseWheelListener, KeyListener {

	private GL2GL3 gl;
	int width, height;

	OGLTextRenderer textRenderer;

	OGLTexture2D textureEarth, textureSun, textureGrass, textureMars, textureMoon, textureSky, textureWater,
			textureWood;

	// BUFFERS - There is problem with garbage collector (method finalize) in OGLBuffers - we need to keep pointer for each buffer. And it is still not enough - after some time, random instance is going to destroy itself.
	OGLBuffers axis, moon, universe;
	List<OGLBuffers> models = new ArrayList<OGLBuffers>(), normals = new ArrayList<OGLBuffers>();

	private int axisProgram, lightProgram, modelProgram, normalProgram, universeProgram;

	Map<Integer, Locations> locations = new HashMap<Integer, Locations>();

	private float time;

	private Mat4 observerProj, lightProj;
	private Camera observer, light;
	private int mx, my;

	boolean info = true, wire = true, mouseMoving;
	String[] modelFragmentShader, modelGeometryShader, modelVertexShader, lightFragmentShader, lightVertexShader;

	String usedFunctions, observerInfo, lightInfo;

	Controller controller;

	OGLRenderTarget zDepthTarget;
	OGLTexture2D.Viewer zDepthViewer;

	public Renderer(Controller controller) {
		this.controller = controller;
	}

	@Override
	public void init(GLAutoDrawable glDrawable) {
		// Check whether shaders are supported
		gl = glDrawable.getGL().getGL2GL3();
		OGLUtils.shaderCheck(gl);

		OGLUtils.printOGLparameters(gl);

		textRenderer = new OGLTextRenderer(gl, glDrawable.getSurfaceWidth(), glDrawable.getSurfaceHeight());

		// Start cameras
		observerRestart();
		lightRestart();

		// Start programs
		/* AXIS program */
		axisProgram = ShaderUtils.loadProgram(gl, "/cviceni/axis.vert", "/cviceni/axis.frag", null, null, null, null);
		locations.put(axisProgram, new Locations() {
			{
				addValue("proj", gl.glGetUniformLocation(axisProgram, "proj"));
				addValue("view", gl.glGetUniformLocation(axisProgram, "view"));
			}
		});
		createAxis();

		/* UNIVERSE program + uniforms */
		universeProgram = ShaderUtils.loadProgram(gl, "/cviceni/universe.vert", "/cviceni/universe.frag", null, null, null, null);

		locations.put(universeProgram, new Locations() {
			{
				addValue("proj", gl.glGetUniformLocation(universeProgram, "proj"));
				addValue("view", gl.glGetUniformLocation(universeProgram, "view"));
				addValue("size", gl.glGetUniformLocation(universeProgram, "size"));
				addValue("sphere", gl.glGetUniformLocation(universeProgram, "sphere"));
				addValue("texture", gl.glGetUniformLocation(universeProgram, "texture"));
			}
		});

		universe = createGrid(controller.getUniverseDensity().getValue());

		/* MODEL, NORMALS program + uniforms */
		modelVertexShader = ShaderUtils.readShaderProgram("/cviceni/model.vert");
		modelGeometryShader = ShaderUtils.readShaderProgram("/cviceni/model.geom");
		modelFragmentShader = ShaderUtils.readShaderProgram("/cviceni/model.frag");
		modelProgram = ShaderUtils.loadProgram(gl, getVertexShader(modelVertexShader, Optional.empty()),
				modelFragmentShader, null, null, null, null);
		reputModelLocations(modelProgram);
		normalProgram = ShaderUtils.loadProgram(gl, getVertexShader(modelVertexShader, Optional.empty()),
				modelFragmentShader, modelGeometryShader, null, null, null);
		reputModelLocations(normalProgram);
		
		models.add(createGrid(controller.getModelDensity().getValue()));

		/* LIGHT program + uniforms as MODEL */
		lightVertexShader = ShaderUtils.readShaderProgram("/cviceni/light.vert");
		lightFragmentShader = ShaderUtils.readShaderProgram("/cviceni/light.frag");
		lightProgram = ShaderUtils.loadProgram(gl, getVertexShader(lightVertexShader, Optional.of(getLightFunctions())),
				lightFragmentShader, null, null, null, null);

		locations.put(lightProgram, new Locations() {
			{
				addValue("proj", gl.glGetUniformLocation(lightProgram, "proj"));
				addValue("view", gl.glGetUniformLocation(lightProgram, "view"));
				addValue("time", gl.glGetUniformLocation(lightProgram, "time"));
				addValue("center", gl.glGetUniformLocation(lightProgram, "center"));
				addValue("texture", gl.glGetUniformLocation(lightProgram, "texture"));
			}
		});

		moon = createGrid(20);

		// Load textures
		textureEarth = new OGLTexture2D(gl, "/cviceni/earth.jpg");
		textureSun = new OGLTexture2D(gl, "/cviceni/sun.jpg");
		textureGrass = new OGLTexture2D(gl, "/cviceni/grass.jpg");
		textureMars = new OGLTexture2D(gl, "/cviceni/mars.jpg");
		textureMoon = new OGLTexture2D(gl, "/cviceni/moon.jpg");
		textureSky = new OGLTexture2D(gl, "/cviceni/sky.jpg");
		textureWater = new OGLTexture2D(gl, "/cviceni/water.jpg");
		textureWood = new OGLTexture2D(gl, "/cviceni/wood.jpg");

		// + Z-test
		gl.glEnable(GL2GL3.GL_DEPTH_TEST);
		zDepthTarget = new OGLRenderTarget(gl, 1024, 1024);
		zDepthViewer = new OGLTexture2D.Viewer(gl);
	}

	void createAxis() {
		float length = 10;
		float[] vertexBufferData = { 0, 0, 0, 0, 0, 0, 0, 0, length, 0, 0, 255,

				0, 0, 0, 0, 0, 0, 0, length, 0, 0, 255, 0,

				0, 0, 0, 0, 0, 0, length, 0, 0, 255, 0, 0 };

		OGLBuffers.Attrib[] attributes = { new OGLBuffers.Attrib("inPosition", 3), // 3 floats
				new OGLBuffers.Attrib("inColor", 3), // 3 floats
		};

		axis = new OGLBuffers(gl);
		axis.addVertexBuffer(vertexBufferData, attributes);
	}

	private String[] getLightFunctions() {
		return controller.getModelFunction().get(Controller.MODEL.CIRCLE).get(Controller.AXIS.CARTESIAN);
	}

	OGLBuffers createGrid(int density) {
		return GridFactory.generateGrid(gl, density, density);
	}

	@Override
	public void display(GLAutoDrawable glDrawable) {
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();
		
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);
		
		// GL - line or fill + projection
		switch (controller.getMode()) {
			case WIRED:
				gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);
				break;
	
			case FILLED:
				gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
				break;
	
			case TEXTURED:
				gl.glTexParameteri(GL2GL3.GL_TEXTURE_2D, GL2GL3.GL_TEXTURE_MIN_FILTER, GL2GL3.GL_NEAREST);
				gl.glTexParameteri(GL2GL3.GL_TEXTURE_2D, GL2GL3.GL_TEXTURE_MAG_FILTER, GL2GL3.GL_NEAREST);
				gl.glTexParameteri(GL2GL3.GL_TEXTURE_2D, GL2GL3.GL_TEXTURE_WRAP_S, GL2GL3.GL_REPEAT);
				gl.glTexParameteri(GL2GL3.GL_TEXTURE_2D, GL2GL3.GL_TEXTURE_WRAP_T, GL2GL3.GL_REPEAT);
				gl.glActiveTexture(GL2GL3.GL_TEXTURE0);
			default:
				break;
		}
		
		if (controller.getCompiles().get(COMPILE.PROJECTION)) {
			projectionReset();
			controller.getCompiles().put(COMPILE.PROJECTION, false);
		}
		
		// SPEED + TIME
		float speed = controller.getSpeed().getValue() / 100f;
		time += time + speed > Math.PI * 100f ? -time + speed : speed;
		
		
		if (controller.isObjectVisible(OBJECT.MODEL) || controller.isObjectVisible(OBJECT.UNIVERSE)) {
			if (controller.getCompiles().get(COMPILE.OBJECT_MODEL)) {
				gl.glDeleteProgram(modelProgram);
				gl.glDeleteProgram(normalProgram);

				modelProgram = ShaderUtils.loadProgram(gl, getVertexShader(modelVertexShader, Optional.empty()),
						modelFragmentShader, null, null, null, null);
				reputModelLocations(modelProgram);

				normalProgram = ShaderUtils.loadProgram(gl, getVertexShader(modelVertexShader, Optional.empty()),
						modelFragmentShader, modelGeometryShader, null, null, null);
				reputModelLocations(normalProgram);
				
				models.add(createGrid(controller.getModelDensity().getValue()));
				controller.getCompiles().put(COMPILE.OBJECT_MODEL, false);
			}
			
			
			// RENDER to zDepth
			if (controller.getLightShadows().getState()) {
				renderModel(modelProgram, true, true);
			} else {
				// If disabled, erase target
				zDepthTarget.bind();
				
				gl.glClearColor(0.3f, 0.0f, 0.0f, 1.0f);
				gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);
			}

			// RENDER model
			renderModel(modelProgram, true, false);
			
			// RENDER normals
			if (controller.isObjectVisible(OBJECT.NORMAL)) {
				renderModel(normalProgram, false, false);				
			}
		}


		if (controller.isObjectVisible(OBJECT.AXIS)) {
			gl.glUseProgram(axisProgram);
			addProjAndViewUniforms(axisProgram);
			axis.draw(GL2GL3.GL_LINES, axisProgram);
		}

		if (controller.isObjectVisible(OBJECT.LIGHT)) {
			gl.glUseProgram(lightProgram);
			addProjAndViewUniforms(lightProgram);
			gl.glUniform1f(locations.get(lightProgram).getValue("time"), time);

			// Move light camera
			Vec3D v = light.getPosition();

			double rotation = speed / Math.PI;

			double x = v.getX() * Math.cos(rotation) + v.getY() * Math.sin(rotation);
			double y = v.getY() * Math.cos(rotation) - v.getX() * Math.sin(rotation);
			lightSetPosition(x, y, v.getZ());
			
			if (controller.getLightIsCamera().getState()) {
				observer = light.forward(Math.PI / 2);
				resetObserverInfo();
			}
			
			gl.glUniform3fv(locations.get(lightProgram).getValue("center"), 1, ToFloatArray.convert(v), 0);
			resetLightInfo();

			// SHOW if not observer
			if (!controller.getLightIsCamera().getState()) {
				// Texture
				gl.glUniform1i(locations.get(lightProgram).getValue("texture"),
						controller.getMode().equals(Controller.MODE.TEXTURED) ? 1 : 0);
				if (controller.getMode().equals(Controller.MODE.TEXTURED)) {
					if (controller.getModel().equals(Controller.MODEL.CIRCLE)) {
						textureSun.bind(lightProgram, "textureID", 0);
					} else {
						textureMoon.bind(lightProgram, "textureID", 0);
					}
				}
			
				moon.draw(GL2GL3.GL_TRIANGLES, lightProgram);
			}
		}
		

		if (controller.isObjectVisible(OBJECT.UNIVERSE) && controller.getModel().equals(Controller.MODEL.CIRCLE)) {
			if (controller.getCompiles().get(COMPILE.OBJECT_UNIVERSE)) {
				universe = createGrid(controller.getUniverseDensity().getValue());
				controller.getCompiles().put(COMPILE.OBJECT_UNIVERSE, false);
			}
			gl.glUseProgram(universeProgram);

			// Uniforms Proj + View and Grid Size
			addProjAndViewUniforms(universeProgram);
			gl.glUniform1f(locations.get(universeProgram).getValue("size"), controller.getUniverseSize().getValue() * 2);
			gl.glUniform1i(locations.get(universeProgram).getValue("sphere"), controller.getModel().equals(Controller.MODEL.CIRCLE) ? 1 : 0);

			// Texture
			gl.glUniform1i(locations.get(universeProgram).getValue("texture"),
					controller.getMode().equals(Controller.MODE.TEXTURED) ? 1 : 0);
			if (controller.getMode().equals(Controller.MODE.TEXTURED)) {
				if (controller.getModel().equals(Controller.MODEL.CIRCLE)) {
					textureSky.bind(universeProgram, "textureID", 0);
				} else {
					textureGrass.bind(universeProgram, "textureID", 0);
				}
			}

			// Draw
			universe.draw(GL2GL3.GL_TRIANGLES, universeProgram);
		}

	
		if (info) {
			textRenderer.drawStr2D(3, height - 12,
					"move - WASD, up/down - QE, pause - Q, zoom - (m) mouse, observe - (l/r) mouse (" + (mouseMoving ? "A" : "N")
							+ "), info - i");

			textRenderer.drawStr2D(3, 27, usedFunctions);
			textRenderer.drawStr2D(3, 15, lightInfo);
			textRenderer.drawStr2D(3, 3, observerInfo);

			zDepthViewer.view(zDepthTarget.getDepthTexture(), -1, 0.25, 0.5);
			zDepthViewer.view(zDepthTarget.getColorTexture(), -1, -0.5, 0.5);
		}
		textRenderer.drawStr2D(width - 122, 3, "richtlu2 & PGRF UHK");
		
	}
	
	// Jestli jde o zDepth, potlacime nektere vypocty; Jestli jde o model, vyuzijeme funkce 
	private void renderModel(int program, boolean model, boolean zDepth) {
		if (program != 0) { // Podarilo se zkompilovat?
			gl.glUseProgram(program);
			
			if (zDepth) {
				zDepthTarget.bind();
				
				gl.glClearColor(0.3f, 0.0f, 0.0f, 1.0f);
				gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

				addProjAndViewUniforms(program, light, lightProj);
			} else {
				gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0);
		        gl.glViewport(0, 0, width, height);
//		        
//				gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//				gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);
				
				addProjAndViewUniforms(program, observer, observerProj);
			}
			
			// Uniforms for model vs/fs
			gl.glUniform1i(locations.get(program).getValue("axisCoor"), controller.getAxisInt());
			gl.glUniform1f(locations.get(program).getValue("time"), -1.0f * time);
			gl.glUniform1f(locations.get(program).getValue("size"), controller.getModelSize().getValue() / 10f);
			gl.glUniform1i(locations.get(program).getValue("isNormal"), model ? 0 : 1);//controller.isObjectVisible(OBJECT.NORMAL) ? controller.getNormalInt() : 0);
			gl.glUniform1i(locations.get(program).getValue("normalType"), controller.isObjectVisible(OBJECT.NORMAL) ? controller.getNormalInt() : 0);
			gl.glUniform1i(locations.get(program).getValue("isZDepth"), zDepth ? 1 : 0);
			gl.glUniformMatrix4fv(locations.get(program).getValue("lightVP"), 1, false, light.getViewMatrix().mul(lightProj).floatArray(), 0);
			
			// Texture
			gl.glUniform1i(locations.get(program).getValue("texture"),
					controller.getMode().equals(Controller.MODE.TEXTURED) ? 1 : 0);
			if (controller.getMode().equals(Controller.MODE.TEXTURED)) {
				switch (controller.getModel()) {
					case FONTAIN:
						textureWater.bind(program, "textureID", 0);
						break;
	
					case CIRCLE:
						textureMoon.bind(program, "textureID", 0);
						break;
	
					case CONE:
						textureEarth.bind(program, "textureID", 0);
						break;
	
					default:
						textureWood.bind(program, "textureID", 0);
						break;
				}

			}

			// Light
			gl.glUniform3fv(locations.get(program).getValue("lightPos"), 1,
					controller.isObjectVisible(Controller.OBJECT.LIGHT) ? ToFloatArray.convert(light.getPosition())
							: ToFloatArray.convert(new Vec3D(0)),
					0);
			gl.glUniform1i(locations.get(program).getValue("lightType"), controller.getLightInt());
			gl.glUniform3fv(locations.get(program).getValue("eyePos"), 1,
					ToFloatArray.convert(observer.getPosition()), 0);

			// Shadows
			if (!zDepth) {
				zDepthTarget.getDepthTexture().bind(program, "depthTexture", 1);				
			}
			
			// Draw - model
			if (controller.isObjectVisible(OBJECT.MODEL)) {
				gl.glUniform1i(locations.get(program).getValue("isModel"), 1);
				models.get(models.size() - 1).draw(GL2GL3.GL_TRIANGLES, program);
			}
			// Draw - grid
			if (!controller.getModel().equals(Controller.MODEL.CIRCLE) && controller.isObjectVisible(OBJECT.UNIVERSE)) {
				gl.glUniform1i(locations.get(program).getValue("isModel"), 0);
				textureGrass.bind(program, "textureID", 0);
				gl.glUniform1f(locations.get(program).getValue("size"), controller.getUniverseSize().getValue());
				models.get(models.size() - 1).draw(GL2GL3.GL_TRIANGLES, program);
			}
			
		}
	}
	

	private void addProjAndViewUniforms(int program) {
		addProjAndViewUniforms(program, observer, observerProj);
	}
	
	private void addProjAndViewUniforms(int program, Camera observer, Mat4 proj) {
		gl.glUniformMatrix4fv(locations.get(program).getValue("view"), 1, false, observer.getViewMatrix().floatArray(),
				0);
		gl.glUniformMatrix4fv(locations.get(program).getValue("proj"), 1, false, proj.floatArray(), 0);
	}

	private void reputModelLocations(int program) {
		locations.put(program, new Locations() {
			{
				addValue("proj", gl.glGetUniformLocation(program, "proj"));
				addValue("view", gl.glGetUniformLocation(program, "view"));
				addValue("axisCoor", gl.glGetUniformLocation(program, "axisCoor"));
				addValue("time", gl.glGetUniformLocation(program, "time"));
				addValue("size", gl.glGetUniformLocation(program, "size"));
				addValue("isNormal", gl.glGetUniformLocation(program, "isNormal"));
				addValue("normalType", gl.glGetUniformLocation(program, "normalType"));
				addValue("isModel", gl.glGetUniformLocation(program, "isModel"));
				addValue("isZDepth", gl.glGetUniformLocation(program, "isZDepth"));
				addValue("texture", gl.glGetUniformLocation(program, "texture"));
				addValue("lightType", gl.glGetUniformLocation(program, "lightType"));
				addValue("lightPos", gl.glGetUniformLocation(program, "lightPos"));
				addValue("lightVP", gl.glGetUniformLocation(program, "lightVP"));
				addValue("eyePos", gl.glGetUniformLocation(program, "eyePos"));
			}
		});
	}

	private String[] getVertexShader(String[] vertexShader, Optional<String[]> functionsToReplace) {
		String[] vertexShaderCopy = new String[vertexShader.length];
		String[] functions;

		if (functionsToReplace.isPresent()) {
			functions = functionsToReplace.get();
		} else {
			functions = new String[] { controller.getAxisTextFields()[0].getText(),
					controller.getAxisTextFields()[1].getText(), controller.getAxisTextFields()[2].getText() };
		}

		for (int i = 0; i < vertexShader.length; i++) {
			vertexShaderCopy[i] = vertexShader[i].replace("funX", functions[0]).replace("funY", functions[1])
					.replace("funZ", functions[2]);
		}

		usedFunctions = "f1: " + functions[0] + "   f2: " + functions[1] + "   f3: " + functions[2];
		return vertexShaderCopy;
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		this.width = width;
		this.height = height;
		textRenderer.updateSize(width, height);
		projectionReset();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mx = e.getX();
		my = e.getY();
		mouseMoving = !mouseMoving;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (mouseMoving) {
			observer = observer.addAzimuth((double) Math.PI * (mx - e.getX()) / width)
					.addZenith((double) Math.PI * -(e.getY() - my) / width);
			mx = e.getX();
			my = e.getY();
			resetObserverInfo();
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getWheelRotation() < 0) {
			observer = observer.forward(Controller.STEP * 3);
		} else {
			observer = observer.backward(Controller.STEP * 3);
		}

		projectionRefresh();
		resetObserverInfo();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {

		// MOVE OBSERVER
		case KeyEvent.VK_W:
			observer = observer.forward(Controller.STEP);
			projectionRefresh();
			resetObserverInfo();
			break;
		case KeyEvent.VK_D:
			observer = observer.right(Controller.STEP);
			resetObserverInfo();
			break;
		case KeyEvent.VK_S:
			observer = observer.backward(Controller.STEP);
			projectionRefresh();
			resetObserverInfo();
			break;
		case KeyEvent.VK_A:
			observer = observer.left(Controller.STEP);
			resetObserverInfo();
			break;
		case KeyEvent.VK_E:
			observer = observer.down(Controller.STEP);
			resetObserverInfo();
			break;
		case KeyEvent.VK_Q:
			observer = observer.up(Controller.STEP);
			resetObserverInfo();
			break;	
			
			
		// RESTART OBSERVER
		case KeyEvent.VK_R:
			observerRestart();
			break;
			
		// OBSERVER on LIGHT position + forward in front of circle
		case KeyEvent.VK_T:
			observer = light.forward(Math.PI / 2);
			resetObserverInfo();
			break;
			
		// SPEED PAUSE
		case KeyEvent.VK_P:
			controller.getSpeed().setValue(0);
			break;
		
		// INFO
		case KeyEvent.VK_I:
			info = !info;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void dispose(GLAutoDrawable glDrawable) {
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();

		for (int program : locations.keySet()) {
			gl.glDeleteProgram(program);
		}
	}

	
	private void observerRestart() {
		// Otoceni do pocatku (pripraveno pro posun kamery na urcitou pozici a
		// dopocitani pohledu k pocatku)
		double xy = 8.0, z = 5;

		observer = new Camera().withPosition(new Vec3D(xy, xy, z))
				.withAzimuth(xy > 0 ? (Math.PI + Math.PI / 4) : (Math.PI / 4)) // doleva ci doprava o 45�
				.withZenith(Math.tan(Math.abs(z / xy)) / 2 * (-z / z)); // dolu ci nahoru

		resetObserverInfo();
	}

	private void lightSetPosition(double x, double y, double z) {
		light = new Camera()
					.withPosition(new Vec3D(x, y, z))
					.withAzimuth(Math.atan2(y,  x ) + Math.PI)
					.withZenith((x * y != 0 ) ? (Math.tan(Math.abs(Math.sqrt(x*x + y*y) / z)) / 2 * (-z / z)) : ( -z / z * Math.PI / 2) );
	}
	
	private void lightRestart() {
		light = new Camera().withPosition(new Vec3D(0, 4, 4)).withAzimuth(Math.PI);

		resetLightInfo();
	}

	private void projectionRefresh() {
		if (controller.getCamera() == CAMERA.ORTHOGONAL) {
			projectionReset();
		}
	}

	private void projectionReset() {
		switch (controller.getCamera()) {
			case PERSPECTIVE:
				observerProj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
				break;
	
			case ORTHOGONAL:
				double ratio = observer.getPosition().getZ();
				observerProj = new Mat4OrthoRH(ratio, ratio, .01, 1000);
				break;
	
			default:
				break;
		}
		lightProj = new Mat4OrthoRH(controller.getUniverseSize().getValue() * 2, controller.getUniverseSize().getValue() * 2, 0.1, light.getPosition().getZ() * Math.PI);
	}

	private void resetObserverInfo() {
		observerInfo = "camera (WASD, Q/E, T(as light), R(estart):   " + getCameraInfo(observer);
	}

	private void resetLightInfo() {
		lightInfo = "light:   " + getCameraInfo(light);
	}

	String getCameraInfo(Camera camera) {
		return "X: " + round(camera.getPosition().getX()) + "   Y: " + round(camera.getPosition().getY()) + "   Z: "
				+ round(camera.getPosition().getZ()) + "   Azimut: " + round(camera.getAzimuth()) + "   Zenith: "
				+ round(camera.getZenith());
	}

	private double round(double x) {
		return Math.round(x * 10.0) / 10.0;
	}

}