#version 150
uniform mat4 proj;
uniform mat4 view;

uniform int axisCoor;
uniform float time;
uniform float size;
uniform mat4 lightVP;
uniform vec3 lightPos;
uniform vec3 eyePos;
uniform int isModel;
uniform int isZDepth;

in vec2 inPosition;
in vec3 inColor;
in vec2 inTextureCoordinates;


out Data {
	vec3 vecPosition;
	vec3 vecNormal;

	float lightDirectionLength;
	vec3 lightDirection;
	vec3 viewDirection;
	vec3 NdotL;

	vec3 vertColor;
	vec2 texCoord;

	vec4 depthTexCoord;
} vs_out;


const float PI = 3.1415;

vec3 sphericalToCartesian(vec3 v) {
    // x = Rho, y = Phi, z = Theta
    float x = v.x * sin(v.z) * cos(v.y);
    float y = v.x * sin(v.z) * sin(v.y);
    float z = v.x * cos(v.z) ;

    return vec3(x, y, z);
}

vec3 cylindricalToCartesian(vec3 v) {
    // x = Rho, y = Phi, z = Z
    float x = v.x * cos(v.y);
    float y = v.x * sin(v.y);
    float z = v.z;

    return vec3(x, y, z);
}

vec3 getVectorFunction(vec3 v){
	float s = (v.y * (2 * PI));
	float t = (PI * v.x);

	return vec3(funX, funY, funZ);
}

vec3 rotateVec(vec3 v){
	float x, y, z;
	float rotation =  time / PI;

	x = v.x * cos(rotation) + v.y * sin(rotation);
	y = v.y * cos(rotation) - v.x * sin(rotation);
	z = v.z;

	return vec3(x, y, z);
}

vec3 multiplyVec(vec3 v){
	float x, y, z;

	if (isModel == 1) {
		x = v.x * size;
		y = v.y * size;
		z = v.z * size;
	} else {
		x = (v.x * size) - (size / 2);
		y = (v.y * size) - (size / 2);
		z = v.z;
	}

	return vec3(x, y, z);
}

// Same transformations as in main! Then cross for separated near vectors.
vec3 getNormal(vec2 p, float z){
	float step = -0.001;
	vec3 v[4];

	v[0] = vec3((p + vec2(step, 0)), z);
	v[1] = vec3((p - vec2(step, 0)), z);

	v[2] = vec3((p + vec2(0, step)), z);
	v[3] = vec3((p - vec2(0, step)), z);

	// 1) TRANSFORMS N - Functions + Coord
	if (isModel == 1) {
		for(int i = 0; i < v.length(); i++){
			v[i] = getVectorFunction(v[i]);
		}

		if (axisCoor == 1) {
			// v = v
		} else if (axisCoor == 2){
			for(int i = 0; i < v.length(); i++){
				v[i] = sphericalToCartesian(v[i]);
			}
		} else if (axisCoor == 3) {
			for(int i = 0; i < v.length(); i++){
				v[i] = cylindricalToCartesian(v[i]);
			}
		}
	}

	// 2) TRANSFORMS N - Rotace
	if (isModel == 1) {
		for(int i = 0; i < v.length(); i++){
			v[i] = rotateVec(v[i]);
		}
	}

	// 3) TRANSFORMS N - Zvetseni
	for(int i = 0; i < v.length(); i++){
		v[i] = multiplyVec(v[i]);
	}

	return cross(v[0] - v[1], v[2] - v[3]);
}


void main() {
	vec2 position = inPosition;
	float z;

	// Ziskani Kartezskych souradnic (tez pro normalu)
	vec3 v, n;

	// 1) TRANSFORMS V - Functions + Coord
	if (isModel == 1) {
		z = 1.0;
		v = getVectorFunction(vec3(position, z));

		if (axisCoor == 1) {
			// v = v
		} else if (axisCoor == 2){
			// Z Sferickych
			v = sphericalToCartesian(v);
		} else if (axisCoor == 3) {
			// Z Cylindrickych
			v = cylindricalToCartesian(v);
		}
	} else {
		z = -0.1;
		v = vec3(position, z);
	}

	// 2) TRANSFORMS V - Rotace
	if (isModel == 1) {
		v = rotateVec(v);
	}

	// 3) TRANSFORMS V - Zvetseni
	v = multiplyVec(v);

	// Barva / Textura
	vs_out.texCoord = inTextureCoordinates;
	vs_out.vertColor = inColor;

	// Pozice bodu
	vs_out.vecPosition = v;
	// Normala bodu
	vs_out.vecNormal = getNormal(position, z);

	vs_out.lightDirection = lightPos - vs_out.vecPosition;
	vs_out.lightDirectionLength = length(vs_out.lightDirection);
	vs_out.viewDirection = eyePos - vs_out.vecPosition;

	vs_out.NdotL = vec3(dot(vs_out.vecNormal, vs_out.lightDirection));


	// Z-buffer
	vs_out.depthTexCoord = lightVP * vec4(vs_out.vecPosition, 1.0);
	vs_out.depthTexCoord.xyz = (vs_out.depthTexCoord.xyz + 1) / 2;

	// Zapsani do bufferu
	gl_Position = proj * view * vec4(vs_out.vecPosition, 1.0);
}
