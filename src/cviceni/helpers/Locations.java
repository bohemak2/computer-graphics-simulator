package cviceni.helpers;

import java.util.HashMap;

public class Locations {
	HashMap<String, Integer> values = new HashMap<String, Integer>();

	public Integer getValue(String key) {
		return values.get(key);
	}

	public void addValue(String key, int value) {
		this.values.put(key, value);
	}
}
