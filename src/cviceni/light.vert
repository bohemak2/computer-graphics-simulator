#version 150
in vec2 inPosition;
in vec3 inColor;
in vec2 inTextureCoordinates;

out vec2 texCoord;

uniform mat4 proj;
uniform mat4 view;
uniform vec3 center;
uniform float time;

const float PI = 3.14;

void main() {
	vec2 position = inPosition;

	// Model
	float s = (position.y * (2 * PI));
	float t =  (PI * position.x);

	vec3 v = vec3(funX, funY, funZ);

	// Rotace telesa podle vlastni osy
	float rotation =  time / PI;
	float x = v.x * cos(rotation) + v.y * sin(rotation);
	float y = v.y * cos(rotation) - v.x * sin(rotation);
	float z = v.z;

	// Posun telesa dle pozice kamery + o velikost telesa
	v.x = x + center.x;
	v.y = y + center.y;
	v.z = z + center.z;



	// Zapsani do bufferu
	gl_Position = proj * view * vec4(v, 1.0);

	texCoord = inTextureCoordinates;
}
