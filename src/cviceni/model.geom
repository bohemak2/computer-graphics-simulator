#version 150
// Typ vstupu a vystupu
layout(triangles) in;
layout(line_strip, max_vertices=6) out;

uniform mat4 proj;
uniform mat4 view;
uniform int normalType;

in Data {
	vec3 vecPosition;
	vec3 vecNormal;

	float lightDirectionLength;
	vec3 lightDirection;
	vec3 viewDirection;
	vec3 NdotL;

	vec3 vertColor;
	vec2 texCoord;

	vec4 depthTexCoord;
} gs_in[];

out Data {
	vec3 vecPosition;
	vec3 vecNormal;

	float lightDirectionLength;
	vec3 lightDirection;
	vec3 viewDirection;
	vec3 NdotL;

	vec3 vertColor;
	vec2 texCoord;

	vec4 depthTexCoord;
} gs_out;


const float N_LENGTH = .25f;

void createLine(vec3 P, vec3 N){
	gl_Position = proj * view * vec4(P, 1.0);
	gs_out.vertColor = vec3(255, 0, 0);
	EmitVertex();

	gl_Position = proj * view * vec4(P + N * N_LENGTH, 1.0);
	gs_out.vertColor = vec3(0, 255, 0);
	EmitVertex();

	EndPrimitive();
}

void main()
{
  if (normalType != 0){
	  for(int i = 0; i < gs_in.length(); i++)
	  {
	    // Pro interface plati, ze co bylo jiz jednou definovano a neprepsano, tak je predano vsem nove emitovanym vrcholum
		gs_out.vecPosition = gs_in[i].vecPosition;
		gs_out.vecNormal = gs_in[i].vecNormal;

		gs_out.lightDirectionLength = gs_in[i].lightDirectionLength;
		gs_out.lightDirection = gs_in[i].lightDirection;
		gs_out.viewDirection = gs_in[i].viewDirection;
		gs_out.NdotL = gs_in[i].NdotL;

		gs_out.vertColor = gs_in[i].vertColor;
		gs_out.texCoord = gs_in[i].texCoord;

		gs_out.depthTexCoord = gs_in[i].depthTexCoord;

		if (normalType == 1){
			// Vrchol
			vec3 P = gs_in[i].vecPosition.xyz;
			vec3 N = normalize(gs_in[i].vecNormal.xyz);

			createLine(P, N);
		} else if (normalType == 2 && (i + 1) % 3 == 0) {
			// Plocha (jednou za predchozi trojuhelnik)
			vec3 P0 = gs_in[i].vecPosition.xyz;
			vec3 P1 = gs_in[i-1].vecPosition.xyz;
			vec3 P2 = gs_in[i-2].vecPosition.xyz;

			vec3 V0 = P1 - P0;
			vec3 V1 = P2 - P0;

			vec3 N = normalize(cross(V1, V0));
			vec3 P = (P0 + P1 + P2) / 3.0;

			createLine(P, N);
		}
	  }
  }
}
