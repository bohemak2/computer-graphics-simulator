package cviceni;



import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.SwingUtilities;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

public class JOGLApp {
	private static final int FPS = 60; // animator's target frames per second

	public void start() {
		try {
			// CONTROLLER - Source of TRUE
			Controller controller = new Controller();
			controller.afterInit();
				
			// FRAME CANVAS
			Frame frameCanvas = new Frame("PGR3 - Canvas");
			
			// OpenGL version
	    	GLProfile profile = GLProfile.getMaximum(true);
	    	GLCapabilities capabilities = new GLCapabilities(profile);
	    	
	    	// The canvas is the widget that's drawn in the JFrame
	    	GLCanvas canvas = new GLCanvas(capabilities) {
	    		/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
	    		public Dimension getSize() {
	    			return Controller.getCanvasSize();
	    		}
	    	};
	    	Renderer ren = new Renderer(controller);
			
	    	canvas.addGLEventListener(ren);
			canvas.addMouseListener(ren);
			canvas.addMouseWheelListener(ren);
			canvas.addMouseMotionListener(ren);
			canvas.addKeyListener(ren);

	    	// Canvas to Frame
	    	frameCanvas.add(canvas);
	    	
	        //shutdown the program on windows close event
	    	final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
	    	 
	    	frameCanvas.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					new Thread() {
	                     @Override
	                     public void run() {
	                        if (animator.isStarted()) animator.stop();
	                        System.exit(0);
	                     }
	                  }.start();
				}
			});
	    	frameCanvas.pack();
	    	frameCanvas.setVisible(true);
            
            
            // FRAME SETTINGS
	    	Frame frameSettings = new Frame("PGR3 - Settings");
	    	frameSettings.add(controller.getSettingsPanel());
			frameSettings.pack();
			frameSettings.setVisible(true);
			frameSettings.setSize(240, 800);
            
            
			// START ANIMATION
			animator.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JOGLApp().start());
	}

}