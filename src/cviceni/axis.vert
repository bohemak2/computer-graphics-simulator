#version 150
uniform mat4 proj;
uniform mat4 view;

in vec3 inPosition; // input from the vertex buffer
in vec3 inColor;
out vec3 vertColor;


void main() {
	vertColor = inColor;

	gl_Position = proj * view * vec4(inPosition, 1.0);
}
