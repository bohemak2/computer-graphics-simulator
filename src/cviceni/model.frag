#version 150
in Data {
	vec3 vecPosition;
	vec3 vecNormal;

	float lightDirectionLength;
	vec3 lightDirection;
	vec3 viewDirection;
	vec3 NdotL;

	vec3 vertColor;
	vec2 texCoord;

	vec4 depthTexCoord;
} fs_in;

uniform int texture;
uniform float size;
uniform vec3 lightPos;
uniform int lightType;
uniform vec3 eyePos;
uniform int isModel;
uniform int isZDepth;

uniform int normalType;
uniform int isNormal;

uniform sampler2D textureID;
uniform sampler2D depthTexture;

out vec4 outColor;

void main() {
	// Ambientni slozka
	vec4 Argb = vec4(vec3(0.5), 1.0);

	// Difuzni slozka
	vec4 Drgb = vec4(fs_in.vertColor, 1.0);

	if (texture > 0 && (normalType == 0 || isNormal == 0)) {
		if (isModel == 1){
			Drgb = texture(textureID, fs_in.texCoord);
		} else {
			Drgb = texture(textureID, mod(fs_in.texCoord * vec2(size, size), vec2(1.0, 1.0)));
		}
	}

	if (isZDepth == 1) {
		// Z test
		outColor = vec4(gl_FragCoord.zzz, 1.0);
	} else if (lightPos.x == 0 && lightPos.y == 0 && lightPos.z == 0){
		// Vypnute svetlo
		outColor = Argb * Drgb;
	} else {
		if (lightType == 1 || lightType == 2){
			vec4 diffuse = vec4(dot(normalize(fs_in.lightDirection), normalize(fs_in.vecNormal)) * vec3(1.0), 1.0);

			vec3 halfVector = normalize(normalize(fs_in.lightDirection) + normalize(fs_in.viewDirection));
			float NdotH = dot(normalize(fs_in.vecNormal), halfVector);
			vec4 specular = vec4(pow(NdotH, 16) *  vec4(vec3(1.0), 1.0));

			// Utlum
			float attenuation = 1.0 / (0.1 + 0.1 * fs_in.lightDirectionLength + 0.1 * fs_in.lightDirectionLength * fs_in.lightDirectionLength);

			// Korekce utlumu
			if (attenuation < 0.1){
				attenuation = 0.1;
			}

			float z1 = texture(depthTexture, fs_in.depthTexCoord.xy / fs_in.depthTexCoord.w).r, z2 = fs_in.depthTexCoord.z / fs_in.depthTexCoord.w;

			if (z1 < z2 - 0.05){
				if (lightType == 1){
					outColor = Drgb * (attenuation * Argb);
				} else if (lightType == 2){
					outColor = Drgb * vec3(0);
				}
			} else {
				vec4 lightColor;

				if (lightType == 1){
					lightColor = Argb + attenuation * (diffuse + specular);
				} else {
					lightColor = Argb + (diffuse + specular);
				}

				outColor = Drgb * lightColor;
			}

		} else if (lightType == 3){
			float intensity = dot(normalize(fs_in.lightDirection), normalize(fs_in.vecNormal));

			if(intensity>0.95) outColor = vec4(1.0,0.5,0.5,1.0);
			else if(intensity>0.8) outColor = vec4(0.6,0.3,0.3,1.0);
			else if(intensity>0.5) outColor = vec4(0.0,0.0,0.3,1.0);
			else if(intensity>0.25) outColor = vec4(0.4,0.2,0.2,1.0);
			else outColor = vec4(0.2,0.1,0.1,1.0);
		}
	}
}
