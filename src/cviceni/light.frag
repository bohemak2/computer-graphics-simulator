#version 150
in vec2 texCoord;

out vec4 outColor;

uniform int texture;
uniform sampler2D textureID;

void main() {
	if (texture > 0) {
		outColor = texture(textureID, texCoord);
	} else {
		outColor = vec4(255, 255, 255, 1.0);
	}
}
