#version 150
in vec3 vertColor; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader
in vec2 texCoord;

uniform int texture;
uniform sampler2D textureID;
uniform float size;

void main() {
	if (texture > 0) {
			outColor = texture(textureID, mod(texCoord * vec2(size, size), vec2(1.0, 1.0)));
		} else {
			outColor = vec4(vertColor, 1.0);
	}
}
