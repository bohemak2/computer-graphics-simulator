K pouziti nutny JOGL.

V pripade vyvolane kompilace nekterym z konfiguracnich prvku muze dochazet ke spadnuti aplikace.
Jedna se o nepredvidatelne chovani, vetsinou spojene s nutnosti znovuzavest shader do graf. karty.
Problem mimo jine zapricinuje zacykleni v oglutils.OGLBuffers v metode finalize, kdy se dostane chybova hlaska do smycky.